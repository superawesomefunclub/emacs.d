(add-to-list 'load-path "/opt/local/share/emacs/site-lisp/color-theme-6.6.0")
     (require 'color-theme)
     (eval-after-load "color-theme"
     	'(progn
     		(color-theme-initialize)
     		(color-theme-deep-blue)))


;;;;;;;;; package

(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)

(setenv "PATH"
  (concat
   "/opt/local/bin" ":"
   (getenv "PATH")
  )
)

;;;;;;;; end of package


(defvar on_darwin
  (string-match "darwin" (prin1-to-string system-type)))

(setq-default transient-mark-mode t)

(global-font-lock-mode t)

(setq font-lock-maximum-decoration t)


;; The following key-binding iconifies a window -- we disable it:
(global-unset-key "\C-x\C-z")


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(menu-bar-mode nil)
 '(package-selected-packages
   (quote
    (eshell-git-prompt better-shell zoom magit powerline ansible-doc ansible projectile-speedbar projectile company-anaconda anaconda-mode company-quickhelp docker-compose-mode focus-autosave-mode yaml-tomato yaml-mode flycheck-rebar3 rainbow-delimiters popup-complete company-racer racer rust-mode flyspell-popup company-distel flycheck-tip flycheck company websocket js2-mode)))
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(tooltip-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
;disable backup
(setq backup-inhibited t)
;disable auto save
(setq auto-save-default nil)


(require 'alchemist)
(global-company-mode)

(focus-autosave-mode)


;;;;;;;;;;;;;;; PYTHON
;(defun my/python-mode-hook ()
;  (add-to-list 'company-backends 'company-jedi))
;(add-hook 'python-mode-hook 'my/python-mode-hook)
(require 'rx)
(eval-after-load "company"
  '(add-to-list 'company-backends 'company-anaconda))
(add-hook 'python-mode-hook 'anaconda-mode)



(projectile-mode +1)
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

(powerline-default-theme)
(eshell-git-prompt-use-theme 'powerline)
